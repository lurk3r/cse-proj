/*
 * qtree.h
 *
 *  Created on: Apr 12, 2014
 *      Author: zrt
 */

#ifndef QTREE_H_
#define QTREE_H_

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <cfloat>
#include <new>
#include <vector>
#include "utility.h"
#include "mortonid.h"
#include <omp.h>
#include <iostream>
#include "userdata.h"
#include <algorithm>

class nbody;
class user_data;

class qtree {
friend class nbody;
friend class euler_tour;
private:
	int *gids;
	char *midids;
	int no;
	int eid;
	qtree **kids;
	qtree *parent;
	int level;
	double *data;//data[x_a,y_a,d_a];
	bool isleaf;
	double *anchor;//anchor[xmin,ymin];
	void get_leaves(std::vector<qtree*> &list);
	void get_nodes(std::vector<qtree*> &list);
	void get_depth(int &depth);
	uint64_t coarse_filling_qdt(uint64_t first, uint64_t last);
	void qdt_to_bqtree(uint64_t *qdts, int num_t, int maxLevel);//construct the base tree used for Par construction of tree
public:
	qtree(qtree *par=NULL, int lev=0, double *anch=NULL);
	virtual ~qtree();
	double width();
	void corners(double cor[4]);//cor[xmin,xmax,ymin,ymax];
	void create_kids();
	void insert_points(int *gids_in,double **points,int nn,int maxPntsPerNode,int maxLevel);
	void insert_points_morton(char *mid_id_in, double **points,int nn, int maxPntsPerNode, int maxLevel);
	void build_tree_par(char *mid_ids,double **points, int no, int maxPntsPerNode, int maxLevel);
	std::vector<qtree*> build_tree_average_par(char *mid_ids,double **points, int no, int maxPntsPerNode, int maxLevel,user_data &ud);
	int* points_in_node(double **points,int *gids_in,int nn,int &nidx);
	char* points_in_node_morton(char *mid_id_in, int nn, int &nidx);
	std::vector<qtree*> leaves();
	std::vector<qtree*> nodes();
	void average_leaves(user_data &ud);
	void average_tree_internal();
	void average_tree_internal_bqtree();
	void average_internal();
	void get_eid_par(std::vector<qtree*> &list);
	void print();
	void print_mids(bool leaves_only=false);
	int depth();
};

#endif /* QTREE_H_ */
