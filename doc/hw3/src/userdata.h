/*
 * userdata.h
 *
 *  Created on: Apr 23, 2014
 *      Author: zrt
 */

#ifndef USERDATA_H_
#define USERDATA_H_

#include "utility.h"

class user_data{
public:
	double **points;
	double *densities;
	int no;
	user_data(double **pnts=NULL, double *den=NULL, int nn=0);
	~user_data();
};

#endif /* USERDATA_H_ */
