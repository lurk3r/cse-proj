/*
 * mortonid.h
 *
 *  Created on: Apr 12, 2014
 *      Author: zrt
 */

#ifndef MORTONID_H_
#define MORTONID_H_

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

class morton_id {
private:
	int tb;
	int lb;
public:
	morton_id();
	virtual ~morton_id();
	uint64_t id(int level, double *anchor);
	void print(uint64_t id);
	double* mid2double(uint64_t);
	uint64_t last_common_ancestor(uint64_t first, uint64_t last);
	uint64_t get_ancestor(uint64_t des, int level);
	uint64_t get_ch_lower(uint64_t mid, int level);
	uint64_t get_ch_upper(uint64_t mid, int level);
	bool is_descendant(uint64_t des, uint64_t ans);
	int get_lb();
	int get_tb();
	void get_ancestor_sibling(uint64_t des,int level, uint64_t *an_sib);
	int get_numbering(uint64_t mid, int level);
};

#endif /* MORTONID_H_ */
