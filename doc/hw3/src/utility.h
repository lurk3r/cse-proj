/*
 * utility.h
 *
 *  Created on: Apr 15, 2014
 *      Author: zrt
 */

#ifndef UTILITY_H_
#define UTILITY_H_
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <cstring>
#include <malloc.h>
#include <math.h>
#include <omp.h>



const double PI = 3.141592653589793;
const double ivPI = 1.0/PI;
#define MERGE_LIM 2

void* unique_merge_sorted(void *a,int sa,void *b,int sb, size_t h,int (*cmp)(const void*,const void*),int &sm);

void* fetch_sub_id(void *a,int sa,size_t h,int *idx,int nidx);

//void unique_merge_sorted_inplace(void *a, int sa,void *b, int sb, size_t h, int (*cmp)(void*,void*),int &sm);

//void* intersect_sorted(void *a,int sa,void *b,int sb, size_t h,int (*cmp)(void*,void*),int &sm);

int intcompare(const void *a,const void *b);

int* mid_id_fetch_id(char* midids, int no);

int mid_id_compare(const void *a, const void *b);

int get_omp_num_threads();

void* ceil_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*));

void* floor_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*));

int id_ceil_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*));

int id_floor_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*));

int last_floor_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*));

int first_ceil_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*));

void qsort_merge_par(void* base, size_t num, size_t size,int (*compar)(const void*,const void*));

void genericScan(void *X, int n, size_t l,void (*ubop)(void * input1andoutput, void *input2));

void userBinaryOperatordouble3d( void *x1, void *x2);

void userBinaryOperatorint( void *x1, void *x2);

void msort (void* base, size_t num, size_t size, int (*compar)(const void*,const void*));

void pmsort (void* base, size_t num, size_t size, int (*compar)(const void*,const void*), size_t threads);

void merge (void* base, size_t num, size_t num1, size_t num2, size_t size, int (*compar)(const void*,const void*));

#endif /* UTILITY_H_ */
