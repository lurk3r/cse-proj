/*
 * nbody.h
 *
 *  Created on: Apr 16, 2014
 *      Author: zrt
 */

#ifndef NBODY_H_
#define NBODY_H_

#include <assert.h>
#include "qtree.h"
#include "utility.h"
#include "mortonid.h"
#include "userdata.h"
#include <omp.h>

//class user_data{
//public:
//	double **points;
//	double *densities;
//	int no;
//	user_data(double **pnts=NULL, double *den=NULL, int nn=0);
//	~user_data();
//};

class euler_tour{
public:
	std::vector<int> I;
	std::vector<int> O;
	euler_tour(std::vector<qtree*> &list, std::vector<int> &subsize);
	~euler_tour();
};


class nbody{
friend class qtree;
private:
//	void selftest();
	void selfclear();
	void get_evaluate(qtree *leaf, qtree *node,user_data &ud,double **trg,double *pot);
	void direct_leaf(double **trg,int nt,double **src,double *dens,int ns,double *pot);
	void direct_internal(double **trg,int nt,double *data,double *pot);
	void average_internal(qtree* node);
	qtree* right_most_descendant(qtree* node);
public:
	double *potential;
	int nop;
	qtree *root;
	int *gids;
	nbody();
	virtual ~nbody();
	void run_nbody(double **points=NULL, double *densities=NULL, int no=0, int maxPntsPerNode=1, int maxLevel=20);
	void run_nbody_par(double **points=NULL, double *densities=NULL, int no=0, int maxPntsPerNode=1, int maxLevel=20);
	void init_mid_id_par(double **points, char *mid_id, int no, int level);
	void selftest();
	std::vector<int> get_subsize_par(std::vector<qtree*> &list);
	void average_tree_internal_par(std::vector<qtree*> &nodes,euler_tour &eutor);
	void evaluate(qtree *leaf,user_data &ud);
	bool well_separated(qtree* source, qtree *target);
	void average_leaves(qtree* node, user_data &ud);
	void average_tree_internal(qtree*node);
	void average_tree_internal_bqtree(qtree* node);
	void print_potential();
};


#endif /* NBODY_H_ */
