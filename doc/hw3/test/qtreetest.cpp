/*
 * qtreetest.cpp
 *
 *  Created on: Apr 17, 2014
 *      Author: zrt
 */

#include <iostream>
#include "../src/qtree.h"

int main(){
	int no,maxPntsPerNode,maxLevel,i;

	no=1 << 6;
	maxPntsPerNode=1;
	maxLevel=20;

	qtree tree;

	int *gids=(int *)malloc(no*sizeof(int));
	for(i=0;i<no;i++){
		gids[i]=i;
	}

	double **pnts=(double**)malloc(no*sizeof(double *));
	for (i=0;i<no;i++){
		pnts[i]=(double*)malloc(2*sizeof(double));
		pnts[i][0]=((double)(i%31))/31.0;
		pnts[i][1]=((double)(i%23))/23.0;
	}

	tree.insert_points(gids,pnts,no,maxPntsPerNode,maxLevel);

	tree.print_mids();

	tree.print_mids(true);

	printf("tree depth is %d\n",tree.depth());

	printf("gids:%0lx\n",gids);
	if (gids!=NULL)
		free(gids);
	for (i=0;i<no;i++){
		free(pnts[i]);
	}
	free(pnts);
}
