\documentclass[PROP,PDF]{prop} % use PDF command to enable PDFLaTeX driver
\usepackage{layout}

\usepackage{geometry}
\geometry{left=2.8cm,right=2.8cm,top=3.5cm,bottom=2cm,headheight=2.5cm,footskip=3.0cm}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{algorithm,algorithmic}
\newcommand{\INDSTATE}[1][1]{\STATE\hspace{#1\algorithmicindent}}
\usepackage{pdfpages}
\usepackage{afterpage}
\usepackage[section]{placeins}
\usepackage{listings}
\usepackage{hyperref}
\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true, breakatwhitespace=true}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}

\title{Parallel Semi-Lagrangian method for advection equation}

\articletype{Project Report} % Research Article, Review Article, Communication, Erratum

\def\checkbox{\indent\text{\rlap{$\checkmark$}}\square}

\def\uncheckbox{\indent\square}



\author{Rongting~Zhang\inst{1}\email{rzhang@math.utexas.edu},
        Yimin~Zhong\inst{1}\email{yzhong@math.utexas.edu}
       }

\shortauthor{R. Zhang, Y. Zhong}

\institute{\inst{1}
           Department of Mathematics, University of Texas at Austin, 78712, Austin, U.S.A
          }

% \abstract{An abstract should accompany every article. It should be a brief summary of significant results of the paper. An abstract should give concise information about the content of the core idea of your paper. It should be informative and not only present the general scope of the paper but also indicate the main results and conclusions.\\
% The abstract should not exceed 200 words. It should not contain literature citations, allusions to the tables, tables, figures or illustrations. All nonstandard symbols and abbreviations should be defined. In combination with the title and keywords, an abstract is an indicator of the content of the paper.
% }

% \keywords{Keyword 1 \*\ Keyword 2 \*\ Keyword 3}

% \msc{XXXXX, YYYYY}

\begin{document}
\maketitle
%\baselinestretch{2}
\section{Problem}
In our project we try to solve the following PDE system with the Semi-Lagrangian approach
\begin{eqnarray}
\dfrac{\partial f}{\partial t} + \mathbf{V}(\mathbf{x},t)\cdot \nabla_x f = 0.
\end{eqnarray}
 
\subsection{Semi-Lagrangian}
The Semi-Lagrangian method requires backtracking during each time step. The mass at time $t_n + \Delta t$ can be tracked from some other mass at $t_n$.
\begin{eqnarray}
f(\mathbf{x}_m,t_n+\Delta t)=f(\mathbf{x}_m-\mathbf{d}_m,t_n).
\end{eqnarray}
Computing $\mathbf{d}_m$ will be accurate if velocity field is constant vector field. Otherwise high order integrator will be needed to evaluate $\mathbf{d}_m$. While there are a lot of feasible methods, we adopt the backward(or implicit) Runge-Kutta method in our implementation.
\begin{eqnarray}
\mathbf{d}_m \sim \int_{t_n}^{t_n + \Delta_t} V(x,t)dt
\end{eqnarray}
\subsection{WENO method}
Since backtracking is performed on the continuous space, there is no guarantee for the tracked location to coincide with grid nodes. Thus interpolation is necessary. And we use the WENO method to preserve shocks while achieving high accuracy. The WENO method is stated as
\begin{align}
u_i^{n+1} &= u_i^{n} - \dfrac{1}{\Delta x}(\mathcal{H}(x_{i+1/2}) - \mathcal{H}(x_{i-1/2}))\\
\mathcal{H}(x_{i+1/2}) &= \mathcal{R}(u_{i-p}^n,\cdots, u_{i+q}^n)
\end{align}
where $\mathcal{H}$ is the interpolation scheme which is controlled by the WENO scheme matrix.
\subsection{Time splitting method}
For one dimensional problem, time splitting is unnecessary, while directly performs backtracking in multiple dimension space will not be easy to implement, thus we need time splitting. During each time step, advection can be performed along each axis. For example ,Strang splitting method performs a second order accuracy scheme in time. For example, 
\begin{eqnarray}
\dfrac{\partial f}{\partial t} + A\dfrac{\partial f}{\partial x} + B \dfrac{\partial f}{\partial y} = 0
\end{eqnarray}
in one time step can be decomposed into 3 sub-1D-problem
\begin{eqnarray}
\dfrac{\partial f}{\partial t} + A\dfrac{\partial f}{\partial x} = 0&\quad&[t_n,t_n + \Delta t/2]\\
\dfrac{\partial f}{\partial t} + B\dfrac{\partial f}{\partial y} = 0&\quad&[t_n, t_n + \Delta t]\\
\dfrac{\partial f}{\partial t} + A\dfrac{\partial f}{\partial x} = 0&\quad&[t_n + \Delta t/2, t_n + \Delta t]
\end{eqnarray}
The result error will be approximated by
\begin{eqnarray}
\dfrac{1}{24}(BA^2+AB^2 + 4BAB- 2ABA-2AB^2 - 2B^2A)\tau^3 + O(\tau^4)
\end{eqnarray}
where $\tau$ is time step length.

\section{Accuracy estimate globally}
From above theories, the resulting error can be written as combination of the three errors.
\begin{eqnarray}
Err_{global} = Err_{rk} + Err_{sp} + Err_{weno} 
\end{eqnarray}
where, $(k+1)$th Runge-Kutta gives order $k$ error in space, $m$th order time split method gives order $m$ error in time, WENO $n$th order scheme gives order $n$ error in space. When requiring high accuracy, the cost on running this schemes will dramatically increase, especially for Runge-Kutta method and time splitting method.
\section{Complexity for Sequential and Parallel}
\subsection{Sequential}
This is pretty straight-forward, at each node, backtracking is performed during each time step. Thus time complexity is $O(M^d N \times T(SL))$, where $T(SL)$ is the cost during updating with Semi-Lagrangian scheme. 

Supposing we use $(k+1)$th Runge-Kutta scheme(thus at least $(k+1)$ components), $m$th order time splitting(roughly $O(m^{d-1})$ steps), $n$th order WENO(uses a $n$ by $n$ matrix multiplication).
\begin{eqnarray}
T(SL) = N(\mbox{Time Split})\times(T(Runge-Kutta) + T(WENO)) \ge O(m^{d-1}kn^2)
\end{eqnarray}
Therefore increasing accuracy order in time will dramatically increase running time for high dimensions.
\subsection{Parallel}
Since each node will update itself independently, this method can be highly parallelized. A PRAM model estimate will give roughly $M^d N T(SL)/p$ time complexity.

\section{Algorithm and implementation}
\subsection{Algorithm}
\begin{algorithm}[H]
\begin{algorithmic}[1]
\FOR{$i=1$ to $N$}
\STATE\hspace{\algorithmicindent}  WENO(U,'X',$t_i$,$\Delta_t/2$)
\STATE\hspace{\algorithmicindent}  WENO(U,'Y',$t_i$, $\Delta_t$)
\STATE\hspace{\algorithmicindent}  WENO(U, 'X', $t_i+\Delta_t/2$, $\Delta_t/2$)
\ENDFOR
\end{algorithmic}
\caption{Semi-Lagrangian WENO solver 2D }
\label{alg:1}
\end{algorithm}
\begin{algorithm}[H]
\begin{algorithmic}[1]
\FOR{(in parallel) $j=1$ to $M$}
\FOR{(in parallel) $k=1$ to $M$}
\STATE\hspace{\algorithmicindent} Shift = BackTrack(V, 'X', $t_i$,$\Delta_t$)
\STATE\hspace{\algorithmicindent} Interpolation(U,Shift)
\ENDFOR
\ENDFOR
\end{algorithmic}
\caption{WENO(Solution\& U, char 'X', double $t_i$, double $\Delta_t$)}
\label{alg:2}
\end{algorithm}
\subsection{Remarks on implementation}
\begin{itemize}
\item Periodical boundary condition, we create template-based cyclic number array data structure for computational use, all basic operations are overloaded for cyclic performance. In this way, WENO scheme can be neat and clean.
\item Parallel Route: OpenMP(shared memory)
\end{itemize}
\section{Accuracy}
 Plot from result: \href{https://bitbucket.org/nlmd/cse-proj/wiki/Home}{https://bitbucket.org/nlmd/cse-proj/wiki/Home}. Figure~\ref{fig:acu_WENO91d}, ~\ref{fig:acu_WENO92d} shows respectively the accuracy of WENO9 for 1D and WENO9 in 2D for constant velocity.
\begin{figure}[htbp]
\includegraphics[scale=0.45]{../final/figure/CWENO1D_9.eps}
\caption{Accuracy of WENO9 in 1D for constant velocity, this is a log-log plot, $x$-axis is mesh size, $y$-axis is the $L_1$ error, fixed CFL number=0.1, (No time splitting or Runge-Kutta needed)}\label{fig:acu_WENO91d}
\end{figure}
\begin{figure}[htbp]
\includegraphics[scale=0.45]{../final/figure/CWENO2D_9.eps}
\caption{Accuracy of WENO9 in 2D for constant velocity, this is a log-log plot, $x$-axis is the mesh size, $y$-axis is the $L_1$ error, fixed CFL number=0.2, (No time spltting or Runge-Kutta needed)}\label{fig:acu_WENO92d}
\end{figure}
\section{Scalability}
\subsection{Weak Scalability}
All figures of this report from 
\href{https://bitbucket.org/nlmd/cse-proj/wiki/Home}{https://bitbucket.org/nlmd/cse-proj/wiki/Home}. Figure~\ref{fig:ws_WENO72d},~\ref{fig:ws_WENO92d},~\ref{fig:ws_WENO73d},~\ref{fig:ws_WENO93d} shows the weak scalability of the scheme using WENO7 in 2D, WENO9 in 2D, WENO7 in 3D, WENO9 in 3D respectively.
\begin{figure}[htp]
\includegraphics[scale=0.35]{../final/figure/2DWENO_7_WSc.jpg}
\caption{Weak Scalability of WENO7 in 2D, $20\times 20$ nodes for each thread, this is a log-log plot, $x$-axis is the number of threads,$y$-axis is the time, using thread 1,2,4,8, using WENO7}\label{fig:ws_WENO72d}
\end{figure}
\begin{figure}[htp]
\includegraphics[scale=0.35]{../final/figure/2DWENO_9_WSc.jpg}
\caption{Weak Scalability of WENO9 in 2D, $20\times 20$ nodes for each thread, this is a log-log plot, $x$-axis is the number of threads,$y$-axis is the time, using thread 1,2,4,8, threads using WENO9}\label{fig:ws_WENO92d}
\end{figure}
\begin{figure}[htp]
\includegraphics[scale=0.35]{../final/figure/3DWENO_7_WSc.jpg}
\caption{Weak Scalability of WENO7 in 3D, $(10\times 10\times 10)$ nodes $\times$ $10$ time steps for each thread, this is a log-log plot, $x$-axis is the number of threads,$y$-axis is the time, using thread 1,2,4 threads using WENO7}\label{fig:ws_WENO73d}
\end{figure}
\begin{figure}[htp]
\includegraphics[scale=0.35]{../final/figure/3DWENO_9_WSc.jpg}
\caption{Weak Scalability of WENO9 in 3D, $(10\times 10\times 10)$ nodes $\times$ $10$ time steps for each thread,  this is a log-log plot, $x$-axis is the number of threads,$y$-axis is the time, using thread 1,2,4 threads using WENO9}\label{fig:ws_WENO93d}
\end{figure}
\FloatBarrier
\subsection{Strong Scalability} 
All figures in this report are from 
\href{https://bitbucket.org/nlmd/cse-proj/wiki/Home}{https://bitbucket.org/nlmd/cse-proj/wiki/Home}. Figure~\ref{fig:ss_WENO72d},~\ref{fig:ss_WENO92d},~\ref{fig:ss_WENO73d},~\ref{fig:ss_WENO93d} shows the strong scalability of the scheme using WENO7 in 2D, WENO9 in 2D, WENO7 in 3D, WENO9 in 3D respectively.
\begin{figure}[htbp]
\includegraphics[scale=0.35]{../final/figure/2DWENO_7_SSc.jpg}
\caption{Strong Scalability of WENO7 in 2D,mesh size $160\times 160$, time step $20$, this is a log-log plot, $x$-axis is the number of threads,$y$-axis is the time, using thread 1,2,4,8 threads using WENO7}\label{fig:ss_WENO72d}
\end{figure}
\begin{figure}[htbp]
\includegraphics[scale=0.35]{../final/figure/2DWENO_9_SSc.jpg}
\caption{Strong Scalability of WENO9 in 2D,mesh size $160\times 160$, time step $20$, this is a log-log plot, $x$-axis is the number of threads,$y$-axis is the time, using thread 1,2,4,8 threads using WENO9}\label{fig:ss_WENO92d}
\end{figure}
\begin{figure}[htbp]
\includegraphics[scale=0.35]{../final/figure/3DWENO_7_SSc.jpg}
\caption{Strong Scalability of WENO7 in 3D,mesh size $40\times 40\times 40$, time steps $40$, this is a log-log plot, $x$-axis is the number of threads,$y$-axis is the time, using thread 1,2,4,8 threads using WENO7}\label{fig:ss_WENO73d}
\end{figure}
\begin{figure}[htbp]
\includegraphics[scale=0.35]{../final/figure/3DWENO_9_SSc.jpg}
\caption{Strong Scalability of WENO9 in 3D, mesh size $40\times 40\times 40$, time steps $40$, this is a log-log plot, $x$-axis is the number of threads,$y$-axis is the time, using thread 1,2,4,8 threads using WENO9}\label{fig:ss_WENO93d}
\end{figure}
\newpage
\section{Bitbucket Repository}
The project is hosted on \href{https://bitbucket.org/nlmd/cse-proj}{https://bitbucket.org/nlmd/cse-proj}.
\newpage
\begin{thebibliography}{6}
\bibitem{QS}
  JingMei Qiu \& Ching-Wang Shu
  \emph{Conservative high order semi-Lagrangian finite difference WENO methods for
  advection in incompressible flow}.
  2009
\end{thebibliography}

% \begin{table}
% \def~{\phantom0}
% \catcode`\@=13
% \def@{\phantom.}
% \caption{Some caption text.\label{tab-01}}
% \medskip
% \begin{center}
% \begin{tabular}{l|ccc}\hline
% \multicolumn1l{\it Some title}\\\hline\hline
% row 1, column 1         &   row 1, column 2  \\
% row 2, column 1    &   row 2, column 2   \\
% row 3, column 1    &   row 3, column 2   \\
% \hline
% \multicolumn1l{\it Another title} & Value 1 &   Value 2 &   Value 3 \\
% \hline\hline
% row 1                    & ~130 & 30 & 30 \\
% row 2                    & 1025 & ~1 & 15 \\
% row 3                   & ~100 & ~1 & 10 \\
% row 4        & 2925 & ~1 & ~4 \\
% row 5            & 2950 & ~1 & ~2 \\\hline
% \end{tabular}
% \end{center}
% \end{table}


% \begin{figure}
% \caption{The graph of $y=x\,sin x$ in the interval [-50,50], created by wxMaxima 0.7.4. \label{fig_abc}}
% %\vskip3cm
% \includegraphics{xsinx-49.png}
% \end{figure}






\end{document}
